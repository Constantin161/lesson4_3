<?php
// функция очищает строку от муссра
function clearStr($str) {
    $str=strip_tags($str);
    $str=stripslashes($str);
    $str=htmlspecialchars($str);
    $str=trim($str);
    return $str;
}

function doRequest($pdo, $sql, $param = array()) {
	$stm = $pdo->prepare($sql);
	$stm->execute($param);
	return $stm->fetchAll();
}

function getUserData($pdo, $login) {
	$sql = "SELECT login, password, id FROM user WHERE login = ?";
    $login = array($login);
    $stm = $pdo->prepare($sql);
	$stm->execute($login);
	return $stm->fetch();
}

function getLoginById($users, $id) {
    foreach ($users as $user) {
        if ($user['id'] != $id) continue;
        return $user['login'];
    }
    return null;
}