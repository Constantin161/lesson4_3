<?php

use lesson4_3\Auth;
use lesson4_3\TaskManager;

include ('config.php');
include ('function.php');
include ('Auth.php');
include ('TaskManager.php');

error_reporting(E_ALL);

session_start();

$pdo = new PDO(DB, LOGIN, PW);

// АВТОРИЗАЦИЯ И РЕГИСТРАЦИЯ

if (isset($_POST['exit'])) {
    session_unset();
    }

if(!isset($_SESSION['login']) || !isset($_SESSION['user_id'])) {
    $status = isset($_POST['sing_in']) ? "sing_in" : (isset($_POST['registr']) ? "registr" : null);
    // Обработка введенных пользователем полей
    $login = isset($_POST['login']) ? clearStr($_POST['login']) : null;
    $pw = isset($_POST['pw']) ? md5(clearStr($_POST['pw'])."brovkot") : null;
    
    $auth = new Auth($pdo, $login, $pw);

    $message = $auth->chooseAction($status);

    $message = !empty($message) ? $message : "Введите данные для регистрации или войдите, если уже зарегистрировались:";

    if(!isset($_SESSION['login'])) {
        include ('auth.html');
        die();
    }
}
// Создаем массив с параметрами юзера
$user['login'] = $_SESSION['login'];
$user['id'] = $_SESSION['user_id'];

// Создаем массив из параметров задачи, передаваемых на сервер
$task['id'] = !empty($_POST['id']) ? $_POST['id'] : null;
$task['description'] = !empty($_POST['description']) ? clearStr($_POST['description']) : null;
$task['assigned_user_id'] = isset($_POST['assigned_user_id']) ? $_POST['assigned_user_id'] : null;

// Определяем тип сортировки
$sorttype = isset($_POST['sorttype']) ? $_POST['sorttype'] : null;

// Определяем вид действие над задачей
$action = isset($_POST['action']) ? $_POST['action'] : null;

$taskManager = new TaskManager($pdo, $user, $task);

$sql = "SELECT id, login FROM user WHERE id != {$user['id']}";
$users = doRequest($pdo, $sql);

$description = $taskManager->doAction($action);

$tasks = $taskManager->showTasks("t.user_id", $sorttype);

$tasksToDo = $taskManager->showTasks("t.assigned_user_id");

include ('form.html');
