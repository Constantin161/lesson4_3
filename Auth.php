<?php

namespace lesson4_3;

class Auth
{
    private $pdo;

    private $login;

    private $pw;

    private $user;


    public function __construct($pdo, $login, $pw)
    {
        $this->pdo = $pdo;
        $this->login = $login;
        $this->pw = $pw;
        $this->user = $this->searchUser($login);
    }

    public function chooseAction($status)
    {
    	if($status === "sing_in") {
    		if (empty($this->login) || empty($this->pw)) {
                return "Ошибка входа. Введите все необходимые данные";
            }
            elseif(!$this->sing_in()) {
                return "Такой пользователь не существует, либо неверный пароль!";
            }
    	}

    	if($status === 'registr') {
	        if (empty($this->login) || empty($this->pw)) {
	            return "Ошибка авторизации. Введите все необходимые данные";
	        }
	        elseif (!$this->createUser()) {
	            return "Пользователь с таким логином уже существует!";
	        }
        }
        return null;
    }


    public function createUser()
    {
        if(empty($this->user)) {
        	$sql = "INSERT INTO user (login, password) 
                    VALUES (?, ?)";
            $authparam = array($this->login, $this->pw);
            $stm = $this->pdo->prepare($sql);
            $stm->execute($authparam);
            $_SESSION['login'] = $this->login;
            $_SESSION['user_id'] = $this->pdo->lastInsertId();
        	return true;
        }
        else {
        	return false; 
        }
    }

    public function sing_in()
    {
        if(($this->login == $this->user['login']) && ($this->pw == $this->user['password'])) {
            $_SESSION['login'] = $this->login;
            $_SESSION['user_id'] = $this->user['id'];
            return true;
        }
        else {
        	return false;
        }
    }

    private function searchUser($login)
    {
        $sql = "SELECT * FROM user WHERE login = ?";
        $stm = $this->pdo->prepare($sql);
        $stm->execute(array($login));
        $user = $stm->fetch();
        return $user;
    }


}
