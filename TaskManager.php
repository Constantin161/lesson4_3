<?php

namespace lesson4_3;

class TaskManager
{
    private $pdo;

    private $task;

    private $user;

    private $sql;

    private $param;

    public function __construct($pdo, $user, $task)
    {
        $this->pdo = $pdo;
        $this->task = $task;
        $this->user = $user;
        $this->param = array($task['id'], $user['id'], $user['id']);
    }

    public function doAction($action)
    {
        switch ($action) {
            case 'add':
                return $this->adTask();
                break;
            case 'change':
                return $this->changeTaskDescription();
                break;
            case 'delete':
                return $this->deleteTask();
                break;
            case 'done':
                return $this->changeTaskStatus();
                break;
            case 'change_assigned':
                return $this->changeAssigned();
                break;
            case 'edit':
               return $this->changeTask();
               break;
        }
    }

    private function adTask()
    {
        $date = date('Y-m-d H:s');
        $this->sql = "INSERT INTO task (description, date_added, user_id) 
                VALUES (?, ?, ?)";                
        $this->param = array($this->task['description'], $date, $this->user['id']);
        return $this->doRequest();
    }

    private function changeTaskDescription()
    {
        $this->sql = "UPDATE task SET description = ? WHERE id = ? AND (user_id = ? OR assigned_user_id =?)";
        array_unshift($this->param, $this->task['description']);
        return $this->doRequest();
    }

    private function deleteTask()
    {
        $this->sql = "DELETE FROM task WHERE id = ? AND (user_id = ? OR assigned_user_id =?)";
        return $this->doRequest();
    }

    private function changeTaskStatus()
    {
        $this->sql = "UPDATE task SET is_done = 1 WHERE id = ? AND (user_id = ? OR assigned_user_id =?)";
        return $this->doRequest();
    }

    private function changeAssigned()
    {
        $this->sql = "UPDATE task SET assigned_user_id = ? WHERE id = ? AND user_id = ?";
        array_pop($this->param);
        array_unshift($this->param, $this->task['assigned_user_id']);
        return $this->doRequest();
    }

    private function changeTask()
    {
        $this->sql = "SELECT description FROM task WHERE id = ? AND (user_id = ? OR assigned_user_id =?)";
        return $this->doRequest();
    }

    public function showTasks($id_param, $sort = "")
    {
        $this->sql = "SELECT t.id, t.description, t.is_done, t.date_added, t.user_id, t.assigned_user_id
        FROM task t
        JOIN user u ON u.id = ".$id_param.
        " WHERE u.id = ?";
        if(!empty($sort)) {
            $this->sql .= " ORDER BY {$sort}";
        }
        $stm = $this->pdo->prepare($this->sql);
        $stm->execute(array($this->user['id']));
        return $stm->fetchAll();
    }

    public function doRequest()
    {
        $stm = $this->pdo->prepare($this->sql);
        $stm->execute($this->param);
        return $stm->fetchAll();
    }
}